package live.auster.staticitems.items.items;

import de.tr7zw.changeme.nbtapi.NBTItem;
import live.auster.staticitems.items.MyItem;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

public class AspectOfTheHoe extends MyItem {
    public final int COOLDOWN = 20;
    public List<Material> NON_TRANSPARENT = new ArrayList<>();

    public AspectOfTheHoe() {
        itemStackName = Component.text("Aspect of the Hoe").color(NamedTextColor.DARK_RED).decoration(TextDecoration.ITALIC, false);
        material = Material.NETHERITE_SWORD;
        canBreakBlocks = false;
        unbreakable = true;

        List<String> names = List.of("CARPET", "SLAB", "TORCH", "VEIN", "BUTTON", "PRESSURE_PLATE", "RAIL");
        for (Material blocktype : Material.values()) {
            if (!List.of(Material.SNOW, Material.TRIPWIRE, Material.STRING, Material.CANDLE, Material.REDSTONE, Material.GLOW_LICHEN, Material.AIR, Material.CAVE_AIR, Material.VOID_AIR).contains(blocktype)) {
                if (blocktype.isAir()) Bukkit.getLogger().log(Level.SEVERE, "fuck fucjk fuck");
                NON_TRANSPARENT.add(blocktype);
                continue;
            }
            for (String n : names) {
                if (!blocktype.name().toLowerCase(Locale.ROOT).contains(n.toLowerCase())) {
                    NON_TRANSPARENT.add(blocktype);
                }
            }
        }
    }

    public void onItemUse(Player var1) {

        var loc = var1.getLocation().clone();
           /* for (int i = 0; i < 20; i++) {
                var loc2 = loc.add(loc.getDirection().
                        multiply(1d + 1d / 20d));
                var blocktype = loc2.getBlock().getType();
                var1.sendMessage(blocktype.name());
                if (NON_TRANSPARENT.contains(blocktype)) break;
                if (NON_TRANSPARENT.contains(Material.AIR)) var1.sendMessage("I EAT A BROOM");
                var1.sendMessage("x");
                loc = loc2;
                loc.getWorld().spawnParticle(Particle.REDSTONE, loc , 10, new Particle.DustOptio<ns(Color.RED, 1));
            }*/
        double REACH = 20d;
        var r = loc.getWorld().rayTraceBlocks(loc, loc.getDirection(), REACH, FluidCollisionMode.NEVER, true);
        var1.sendMessage((r != null) + "");

        loc = r != null ? (r.getHitPosition().toLocation(loc.getWorld(), loc.getYaw(), loc.getPitch()))
                : loc.add(loc.getDirection().
                multiply(REACH));
        var1.teleport(loc);
        loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 10, new Particle.DustOptions(Color.RED, 1));
        var1.playSound(var1.getEyeLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 0.4f, 0.3f);


    }


    @Override
    public void onItemStackCreate(ItemStack var1) {
        NBTItem item = new NBTItem(var1, true);
        item.setInteger("durability", 100);

    }

    @Override
    public ShapedRecipe setRecipe(ShapedRecipe recipe) {
        return null;
    }

    @Override
    public void leftClickAirAction(Player var1, ItemStack var2) {
        Bukkit.broadcastMessage(NON_TRANSPARENT.contains(Material.AIR) + " ");
        //TRANSPARENT.stream().map(Material::name).forEach(Bukkit::broadcastMessage);
    }

    @Override
    public void leftClickBlockAction(Player var1, PlayerInteractEvent var2, Block var3, ItemStack var4) {

    }

    @Override
    public void rightClickAirAction(Player var1, ItemStack var2) {

    }

    @Override
    public void rightClickBlockAction(Player var1, PlayerInteractEvent var2, Block var3, ItemStack var4) {

    }

    @Override
    public void shiftLeftClickAirAction(Player var1, ItemStack var2) {

    }

    @Override
    public void shiftLeftClickBlockAction(Player var1, PlayerInteractEvent var2, Block var3, ItemStack var4) {


    }

    @Override
    public void shiftRightClickAirAction(Player var1, ItemStack var2) {
        onItemUse(var1);

    }

    @Override
    public void shiftRightClickBlockAction(Player var1, PlayerInteractEvent var2, Block var3, ItemStack var4) {
        onItemUse(var1);

    }

    @Override
    public void hitEntityAction(Player var1, EntityDamageByEntityEvent var2, Entity var3, ItemStack var4) {

    }

    @Override
    public void breakBlockAction(Player var1, BlockBreakEvent var2, Block var3, ItemStack var4) {

    }

    @Override
    public void interactEntity(Player var1, PlayerInteractAtEntityEvent var2, Entity var3, ItemStack var4) {

    }
}